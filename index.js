


Vue.component ('startbutton', {
    props : ['order', 'level'],
    template : `<button v-on:click="startGame()">Start</button>`,
    methods : {
        startGame() {
            this.$emit('start-game');
        }
    }
});

var app = new Vue({
    el: '#simon',
    data : {
        level : 0,
        // Liste des puddles à afficher
        puddles : [
            { text: "one" , color : "red", classActive: "red-active", active:false, sound:new Audio("sounds/red.ogg")},
            { text: "two" , color : "yellow", classActive: "yellow-active", active:false, sound:new Audio("sounds/yellow.ogg")},
            { text: "three" , color : "green", classActive: "green-active", active:false, sound:new Audio("sounds/green.ogg")},
            { text: "four" , color : "blue", classActive: "blue-active", active:false, sound:new Audio("sounds/blue.ogg")},
        ],
        order : [],
        orderPlayer : 0
    }, 
    methods : {
        // Initialise une partie
        startGame: function() {
            this.level = 0;
            this.order = [];
            this.levelUp()
        },
        // Augmente la difficulté
        levelUp: function() {
            rand = Math.floor(Math.random() * (3 - 0 +1)) ;
            this.level++;
            this.orderPlayer = 0;
            this.order.push(rand);
            // Pour laisser le temps aux Puddles d'être généré
            const that = this;
            setTimeout(function() {
                that.highlightsPuddles();
            }, 500);
        },

        // Met en surbrillances les différents puddles à sélectionner
        highlightsPuddles: function () {
            let ref;
            for (var i = 0; i < this.order.length; i++) {
                this.$refs.puddleObject[this.order[i]].highlightsSynchro(i);
            }
        },
        // Clique sur un puddle
        puddleClick : function(id) {
            if (this.order[this.orderPlayer] == id) {
                const that  = this;
                this.$refs.puddleObject[id].highlight();
                this.orderPlayer++;
                // Check End
                if (this.orderPlayer == this.order.length) {
                    this.levelUp();
                }
            } else {
                alert("Tu t'es trompe !!! Tu recommences !");
                this.startGame();
            }
        }
    }
})