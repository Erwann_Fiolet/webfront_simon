
Vue.component ('level', {
    props : ['level'],
    template : `<h1 id="level"><slot></slot> {{ level }} </h1>`
});