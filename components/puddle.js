Vue.component ('puddle', {
    props : ['data', 'index'],
    template : `<div v-bind:class="[data.color, data.active ? data.classActive : '']"  class="puddle" v-on:click="puddleClick(index)" ></div>`,
    methods : {
        puddleClick(id) {
            this.$emit('click-button', id);
        },
        // Synchronise les puddles à mettre en surbrillance
        highlightsSynchro(i) {
            const that  = this;
            setTimeout(function(){
                that.highlight();
            } ,1000 * (i+2));
        },
        // Met en surbrillance le puddle d'indice i dans l'ordre des puddles
        highlight() {
            const that  = this;
            this.data.active = true;
            this.data.sound.play();
            setTimeout(function(){
                that.data.active = false;
            }, 900);
        }
        
    }
});